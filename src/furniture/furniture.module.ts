import { Module } from '@nestjs/common';
import { FurnituresService } from './furniture.service';
import { FurnituresController } from './furniture.controller';
import { Furniture } from './furniture.model';
import { SequelizeModule } from '@nestjs/sequelize';



@Module({
  imports: [SequelizeModule.forFeature([Furniture])],
  providers: [FurnituresService],
  controllers: [FurnituresController],
  exports: [SequelizeModule]
})
export class FurnituresModule {}