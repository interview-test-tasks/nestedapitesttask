import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Furniture } from './furniture.model';


@Injectable()
export class FurnituresService {

  isFirstTime;

  constructor(
    @InjectModel(Furniture)
    private furnitureModel: typeof Furniture,
  ) {
    this.isFirstTime = true
  }

  async getTable() {

    if (this.isFirstTime){
      await this.insertEntry({name: 'Furniture', parentId : '0'})
      await this.insertEntry({name: 'Bedroom', parentId : '1'})
      await this.insertEntry({name: 'Bedside table', parentId : '2'})
      await this.insertEntry({name: 'Dressing table', parentId : '2'})
      await this.insertEntry({name: 'Living room', parentId : '1'})
      await this.insertEntry({name: 'Coffee table', parentId : '5'})
      await this.insertEntry({name: 'Chair', parentId : '5'})
      await this.insertEntry({name: 'Bed', parentId : '2'})
      await this.insertEntry({name: 'Armchair', parentId : '5'})
      await this.insertEntry({name: 'Dinning room', parentId : '1'})
      await this.insertEntry({name: 'Dinning room table', parentId : '10'})
      this.isFirstTime = false
    }

    let rawResult = await this.furnitureModel.findAll()

    let mappedResult = rawResult.map(({id, name, parentId}) => {return {id, name, parentId}})
    mappedResult.reverse()

    let childedResult = []

    while (mappedResult.length > 0){
      let element = mappedResult.shift()
      let childedElement = {
        id: element.id,
        name: element.name,
        parentId : element.parentId,
        children: []
      }
      for (let child of childedResult) {
        if (child.parentId == childedElement.id){
          childedElement.children.push(child)
        }
      }
      childedElement.children.reverse()
      childedResult.push(childedElement)
    }
    return childedResult.pop()
  }

  async insertEntry({name, parentId}) {
    return await this.furnitureModel.create(
      {name: name, parentId: parentId}
    )
  }
}