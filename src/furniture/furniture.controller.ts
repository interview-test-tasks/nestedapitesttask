import { Body, Controller, Get, Post, Response } from '@nestjs/common';
import { FurnituresService } from './furniture.service';


@Controller()
export class FurnituresController {
  constructor(private readonly furnituresService: FurnituresService) {}

  @Get('table')
  async getTable(@Response() res) {
    let jsonResponse = await this.furnituresService.getTable()
    res.send(jsonResponse)
  }

  @Post('table')
  async postTable(@Body() body, @Response() res) {
    let result = await this.furnituresService.insertEntry(body)
    res.send(result)
  }
}
