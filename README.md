# NestedAPITestTask

For Programmnie Technologii LLC.
Test task: Create an API (Nested) with just one table having categories (id, name, parentId) and
the api response should be having nested children upto n levels.
Stack: TS+NestJS+MySQL

## Documentation

### API

#### GET /

Returns Hello World! string.

#### GET /table

Returns nested JSON of format:

{
    id: int,
    name: string,
    parentId: int,
    children: [
        {
            id: int
            name: string,
            parentId: int
            children: [...]
        },...
    ],
}

##### POST /table

Takes request' JSON body of format:

{
    name: string,
    parentId: int,
}

Posts entry into database

Returns JSON of inserted entry of format:

{
    id: int,
    name: str,
    parentId: int,
    updatedAt: datetime,
    createdAt: datetime
}
}
